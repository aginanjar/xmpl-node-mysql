// # Modules
const mysql = require('mysql');
const config = require('./config');
const conn = mysql.createConnection(config);
const doFetch = require('./app/helpers/httpclient');

// # Models
const salesItem = require('./app/models/salesItem');
const accToken = require('./app/models/accToken');

// # Logger
const winston = require('winston');
const logger = winston.createLogger({
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'combined.log' })
  ]
});

// Concurrent API
let urls = require('./list-api') || [];
if(process.argv.slice(2).length > 0) {
  let list = './' + process.argv.slice(2)[0];
  urls = require(list);
}

let accounts = require('./list-accounts') || [];
if (process.argv.slice(3).length > 0) {
  let accs = './' + process.argv.slice(3)[0];
  accounts = require(accs);
}

const promises = [];

// # Process(es)
conn.connect(function(err) {
  if(err) throw err;
 
  let condition = false;
  let limit = false; // how much limitation will be displayed
  
  t = new accToken(conn, accounts.toString());
  let tokens = t.getAll(condition, limit, (err, data) => {
    if (err) {
      // error handling code goes here
      console.log("ERROR : ", err);
    } else {
      data.map((value,index) => {
        promises.push(doFetch(urls[0],value.api_token));
        Promise.all(promises)
        .then(results => {
          logger.log({
            level: 'info',
            date: new Date().toISOString(),
            message: "Outlet report of account ID : "+value.account_id + " was generated.",
            data: results
          });
        })
        .catch(err => {
          logger.log({
            level: 'error',
            date: new Date().toISOString(),
            message: "Outlet report of account ID : " + value.account_id + " failed generated.",
            data: err
          });
        });
      });
    }
  });
 
  conn.end(); 
});
