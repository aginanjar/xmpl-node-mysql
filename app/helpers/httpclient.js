require('es6-promise').polyfill();
require('isomorphic-fetch');

module.exports = (url,token) => {
  return fetch(url, {
    method: "POST",
    headers: {
      "token": token,
      "Content-Type": "multipart/form-data"
    }
  })
  .then(result => {
    console.log('API hit...');
    return result.json();
  })
  .catch(err => err);
};

