const accToken = function (conn, accounts) {
    this.connection = conn;
    this.accounts = accounts;

    this.getAllQuery = `select account_id, api_token from(
            select account_id, created_date, api_token
        from account_user_token
        where account_id IN(${this.accounts})
        order by account_id asc, created_date desc
        ) x
        group by account_id`;
};

accToken.prototype.getAll = function (condition = false, limit = false, callback) {
    let query = this.getAllQuery
        + ((condition) ? condition : '')
        + ((limit) ? ` LIMIT ${limit}` : '');

    this.connection.query(query, function (err, result, fields) {
        if (err) callback(err, null);

        // Print result
        callback(null, result );
    });
};

module.exports = accToken;
