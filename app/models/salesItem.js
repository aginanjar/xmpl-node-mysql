const salesItem = function(conn) {
  this.connection = conn;
};
salesItem.prototype.getAllQuery = 'SELECT sales_item_id as salesItemId, shop_id as shopId, user_id as userId, device_id as deviceId, table_no as tableNo, receipt_no as receiptNo, sales_no as salesNo, report_no as reportNo, sales_date as salesDate, sales_time as salesTime, coverage as coverage, split_number as splitNumber, sales_ref as salesRef, item_sales_ref as itemSalesRef, table_status as tableStatus, sales_category_id as salesCategoryId, group_id as groupId, department_id as departmentId, department_level as departmentLevel, product_id as productId, topping_id as toppingId, topping_item_id as toppingItemId, product_status as productStatus, item_plu_no as itemPluNo, item_seq_number as itemSeqNumber, item_quantity as itemQuantity, item_name as itemName, item2nd_name as item2ndName, item_amount as itemAmount, payment as payment, change_amount as changeAmount, gratuity as gratuity, adjustment as adjustment, discount_name as discountName, discount_percentage as discountPercentage, discount as discount, tax_id as taxId, tax_type as taxType, tax_base as taxBase, promotion_id as promotionId, promotion as promotion, recursion_index as recursionIndex, promotion_user_id as promotionUserId, is_buy_x_free_y as isBuyXFreeY, is_stock as isStock, is_consumable as isConsumable, is_redeem_item as isRedeemItem, redeem_points as redeemPoints, trans_mode as transMode, trans_status as transStatus, refund_id as refundId, feature_id as featureId, subfeature_id as subfeatureId, members_id as membersId, member_card_no as memberCardNo, customer_info as customerInfo, card_scheme as cardScheme, credit_card_no as creditCardNo, recipe_id as recipeId, price_shift as priceShift, shift_id as shiftId, is_prep_item as isPrepItem, is_print_free_prep as isPrintFreePrep, is_print_prep_price as isPrintPrepPrice, is_track_prep_item as isTrackPrepItem, is_foc_item as isFocItem, foc_name as focName, foc_user_id as focUserId, link_to as linkTo, rnd_adjustment as rndAdjustment, is_set_menu as isSetMenu, set_menu_ref as setMenuRef, is_post_send_void as isPostSendVoid, is_table_hold as isTableHold, deposit_id as depositId, transfer_sales_no as transferSalesNo, transfer_table_no as transferTableNo, transfer_sales_ref as transferSalesRef, transfer_user_id as transferUserId, is_rental_item as isRentalItem, start_rental_time as startRentalTime, end_rental_time as endRentalTime, rental_minutes as rentalMinutes, seat_number as seatNumber, is_show_rent_warning as isShowRentWarning, sales_area_code as salesAreaCode, business_date as businessDate, first_user_id as firstUserId, cc_promo1 as ccPromo1, cc_promo2 as ccPromo2, voucher_seq_number as voucherSeqNumber, server_number as serverNumber, table_served_time as tableServedTime, served_status as servedStatus, has_comments as hasComments, cpr_voucher_no as cprVoucherNo, foreign_paid_amount as foreignPaidAmount, tax_tag as taxTag, captain_order_no as captainOrderNo, is_print_kds as isPrintKds, void_reason_id as voidReasonId, note as note, parent_id as parentId, item_type as itemType, item_sub_type as itemSubType, product_type as productType, category_id as categoryId, uom_id as uomId, uom as uom, value as value, subtotal as subtotal, sales_category_name as salesCategoryName, payment_id as paymentId, card_valid_until as cardValidUntil, promotion_priority as promotionPriority, promotion_type as promotionType, is_special_price as isSpecialPrice, x_quantity as xQuantity, y_quantity as yQuantity, tax_amount as taxAmount, tax_priority as taxPriority, is_calculate_changes as isCalculateChanges, is_donation as isDonation, is_apply_to_subtotal as isApplyToSubtotal, is_automatic as isAutomatic, price_action_type as priceActionType, rev_number as revNumber, is_deleted as isDeleted, created_by as createdBy, created_date as createdDate, updated_by as updatedBy, updated_date as updatedDate, business_session_date as businessSessionDate FROM sales_item';

salesItem.prototype.getAll = function(condition = false, limit = false) {
  let query = this.getAllQuery 
    + (( condition ) ? condition : '' )
    + (( limit ) ? ` LIMIT ${limit}` : ''); 
  
  this.connection.query(query, function(err, result, fields){
    if(err) throw err;

    // Print result
    console.log(JSON.stringify(result));
    
    // Write to file
    // fs.writeFile('salesitem.json', JSON.stringify(result), function(e) {
    //    if(e) throw e;
    // });

  });
}; 

module.exports = salesItem;
